package main

import (
	"fmt"
	"gitlab.com/busforce/caliprise/accountservice/service" // NEW
)

var appName = "accountservice"

func main() {
	fmt.Printf("Starting %v\n", appName)
	service.StartWebServer("6767") // NEW
}
